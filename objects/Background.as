﻿package objects
{
	import flash.display.Stage;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	
	
	public class Background extends Sprite
	{
		private var stars1:Stars;
		private var stars2:Stars;
		
		public function Background()
		{
			stars1 = new Stars();
			stars1.y = 0;
			
			addChild(stars1);
			
			stars2 = new  Stars();
			stars2.y = -434;
			addChild(stars2);			
		}
		
		public function update():void
		{
			stars1.y += 2;
			if(stars1.y >= 434)
				stars1.y = -434;
			stars2.y += 2;
			if(stars2.y >= 434)
				stars2.y = -434;
		}

	}
}