﻿package objects {
	
	import flash.display.MovieClip;
	import flash.events.TimerEvent
	import flash.utils.Timer;
	
	import Game;
	import objects.Missile;
	
	
	public class Alien extends MovieClip {
		
		private var game:Game;
		private var myTimer:Timer;
		
		public function Alien(game:Game) {
			
			this.game = game;
			
			myTimer= new Timer(1000);
			myTimer.addEventListener(TimerEvent.TIMER, timerListener);
			myTimer.start();


		}
		
		private function timerListener (e:TimerEvent):void{
			if (Math.random() < .01 / this.game.alienMissiles.length)
			{
				launchMissile();
				
			}
		}

		
		public function launchMissile() {
			
			
			var missile:Missile = new Missile();
			missile.x = this.x + this.game.aliensContainer.x;
			missile.y = this.y + this.game.aliensContainer.y + 20;
			trace("alien launch at :" + missile.x + ":" + missile.y);
			this.game.alienMissiles.push(missile);
			
			this.game.addChild(missile);
			  
			}
			
			
		public function die() {
			myTimer.stop();
			}
		}
		
		
			
	}
	

