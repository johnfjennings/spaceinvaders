﻿package  {
	
	import flash.display.MovieClip;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	import fl.transitions.Tween;
	import fl.transitions.easing.*;
	import fl.transitions.TweenEvent;
	import flash.media.Sound;
	
	import objects.Background;
	import objects.Alien;
	import objects.Missile;
	
	import flash.text.TextField;
	
	
	
	public class Game extends MovieClip {
		
		private const SPEED:int = 4; // SpaceShip speed
		
		private const MAX_MISSILES:int = 2; // manimum # of  hero missiles in the air 
		
		private var alienSpeed:Number = 1.0; // starting alien speed 
		
		private var background:Background; // animated background
		private var aliens:Array;	// array to store aliens 
		private var missiles:Array; // array to store hero missiles 
		public var alienMissiles:Array; // array to store alien missiles
		public var aliensContainer:MovieClip; // container to store aliens and treat as one object
		private var aliensDirection:int = 1; // toggle between 1 and -1 to change alien direction
		
		private var wallLeft:Wall; // left boundary for aliens
		private var wallRight:Wall; // right boundary for aliens
		
		private var spaceShip:SpaceShip; // hero
		private var heroLives:int = 3; 
		
		private var speedX = 0; // speed that hero is moving left or right
		private var speedY = 0; // not used at present
		
		private var launchSound:Sound; 
		private var killSound:Sound; 
		
		private var scoreText:TextField;
		private var livesText:TextField;
		private var score:int = 0;
		
		public function Game() {
			//Wait until the Game class has been insantiated  and added to stage before executing main code 
			this.addEventListener(Event.ADDED_TO_STAGE,init);
			
		}
		
		private function init(e:Event) {
			
			background = new Background();
			this.addChild(background);
			
			spaceShip = new SpaceShip();
			spaceShip.x=50;
			spaceShip.y=380;
			this.addChild(spaceShip);
			
			aliens = new Array();
			missiles = new Array();
			alienMissiles = new Array();
			aliensContainer = new MovieClip();
			
			setupAliens();
			
			wallLeft = new Wall();
			wallLeft.x = -20;
			wallLeft.y = 0;
			addChild(wallLeft);
			
			wallRight = new Wall();			
			wallRight.x = 580;
			wallRight.y = 0;
			addChild(wallRight);			

			var scoreFormat:TextFormat = new TextFormat();
			scoreFormat.size = 24;
			scoreFormat.bold = true;
			scoreFormat.color = 0xFFFFFF;
			
			scoreText = new TextField();
			scoreText.defaultTextFormat = scoreFormat;
			scoreText.x = 420;
			scoreText.y = 10;
			scoreText.width = 150;
			scoreText.text="SCORE: " + score.toString();
			this.addChild(scoreText);
			
			livesText = new TextField();
			livesText.defaultTextFormat = scoreFormat;
			livesText.x = 10;
			livesText.y = 10;
			livesText.width = 150;
			livesText.text="LIVES: " + heroLives.toString();
			this.addChild(livesText);			
			
			launchSound = new Shoot(); 
			killSound = new Killed(); 
			
			addEventListener(Event.ENTER_FRAME, update);
			stage.addEventListener(KeyboardEvent.KEY_DOWN,handleKeyDown);
			stage.addEventListener(KeyboardEvent.KEY_UP ,handleKeyUp);
		}
		
		private function setupAliens() {
			
			aliensContainer.x = 0;
			aliensContainer.y = 15;
			this.addChild(aliensContainer);
			
			for (var k:int=0; k < 4;k++){
				for (var j:int=0; j < 10;j++){
					var alien:Alien = new Alien(this);
					aliens.push(alien);
					alien.x = 20+j*50;
					alien.y = 20+k*45;

					aliensContainer.addChild(alien);
					trace("Addes Alien at " + alien.x + ":" + alien.y);
				}
			}
		}
		
		
		private function handleKeyDown(e:KeyboardEvent) {
			if ((e.keyCode == 37) && (spaceShip.x > 50)) {
				speedX = -SPEED;
			}

			if ((e.keyCode == 39)  && (spaceShip.x < 445)) {
				speedX = SPEED;
			}

			if(e.keyCode == Keyboard.SPACE) {
				launchMissile();
			}
		}
		
		public function handleKeyUp(e:KeyboardEvent) {
			if(e.keyCode == 37 || e.keyCode == 39) {
				speedX = 0;
			}
			if(e.keyCode == 38 || e.keyCode == 40) {
				speedY = 0;
			}
		}

		private function launchMissile() {
			
			if (missiles.length < MAX_MISSILES) {
				var missile:Missile = new Missile();
				missile.x = spaceShip.x;
				missile.y = spaceShip.y;
				this.addChildAt(missile,1);
				missiles.push(missile);
				launchSound.play();	   
			}
		}
		
		private function update(e:Event) {
			
			background.update();
		
			if (spaceShip.x < 50) {
				spaceShip.x = 51;
				speedX = 0;
			} else if ( spaceShip.x > 505) {
				spaceShip.x = 504; 
				speedX = 0;
			}
			
			spaceShip.x += speedX;
			
			aliensContainer.x += alienSpeed*aliensDirection*Math.ceil(1/aliens.length);

			if ((aliensContainer.hitTestObject(wallLeft) ) || (aliensContainer.hitTestObject(wallRight) ) ) {
				aliensDirection = -aliensDirection;
				aliensContainer.y +=5;
				alienSpeed *= 1.02;
			}
			
			for (var i:int=0; i<missiles.length;i++){
				missiles[i].y-=5;
				if (missiles[i].y < -5) {
					this.removeChild(missiles[i]);
					missiles[i] = null;
					missiles.splice(i,1);
				}
			}
			
			for (var l:int=0; l<alienMissiles.length;l++){
				alienMissiles[l].y+=5
				if (alienMissiles[l].hitTestObject(spaceShip)) {
					//Lose life
					heroLives--;
					livesText.text="LIVES: " + heroLives.toString();
					this.removeChild(alienMissiles[l]);
					alienMissiles[l] = null;
					alienMissiles.splice(l,1);
				}
				if (alienMissiles[l].y > 400) {
					this.removeChild(alienMissiles[l]);
					alienMissiles[l] = null;
					alienMissiles.splice(l,1);
				}
			}
			
			for (var j:int=0; j<aliens.length;j++){
				for (var k:int=0; k<missiles.length;k++) {
					if((aliens[j].hitTestObject(missiles[k]))  && (aliens[j].visible==true )){
						//aliens[j].visible = false;
						aliens[j].die();
						aliensContainer.removeChild(aliens[j]);
						aliens[j] = null;
						aliens.splice(j,1)
						killSound.play();
						score++;
						scoreText.text="SCORE: " + score.toString();
						trace("SCORE: " + score); 
						this.removeChild(missiles[k]);
						missiles.splice(k,1);
						alienSpeed *= 1.01;
					}
				}
			}
		
			if (aliens.length == 0) {
				setupAliens();
			}
		}
		
		
	}
	
}
